/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import javax.persistence.EntityManager;
import model.Comanda;
import utils.Singleton;

/**
 *
 * @author johnn
 */
public class ComandaDAO {
    private EntityManager em;
    
    public ComandaDAO(){
        em = Singleton.getConnection();
    }
    
    public int inserir(Comanda c){
        em.getTransaction().begin();
        em.persist(c);
        em.flush();
        em.getTransaction().commit();
        return c.getIdComanda();
    }
}
