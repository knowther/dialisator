/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import utils.Singleton;
import model.Paciente;
/**
 *
 * @author johnn
 */
public class PacienteDAO {
    private EntityManager em;
    
    public PacienteDAO(){
    em = Singleton.getConnection();
}
    public void inserir (Paciente pac){
        em.getTransaction().begin();
        em.persist(pac);
        em.getTransaction().commit();
    }
    public void excluir(Paciente pac){
        em.getTransaction().begin();
        em.remove(pac);
        em.getTransaction().commit();
    }
    
    public List getListPac(){
       em.getTransaction().begin();
       Query query = em.createQuery("SELECT p from Paciente p");
       List<Paciente> lista = query.getResultList();
       em.getTransaction().commit();
       return lista;
    }
}


