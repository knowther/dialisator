/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import javax.persistence.EntityManager;
import model.DetalheComanda;
import utils.Singleton;

/**
 *
 * @author johnn
 */
public class DetalheComandaDAO {
    private EntityManager em;
    
    public DetalheComandaDAO(){
        em = Singleton.getConnection();
    }
    
    public void inserir(DetalheComanda dc){
        em.getTransaction().begin();
        em.persist(dc);
        em.getTransaction().commit();
    }
    
}
