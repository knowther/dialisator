/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author johnn
 */
@Entity
@Table(name = "comanda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Comanda.findAll", query = "SELECT c FROM Comanda c")
    , @NamedQuery(name = "Comanda.findByIdComanda", query = "SELECT c FROM Comanda c WHERE c.idComanda = :idComanda")
    , @NamedQuery(name = "Comanda.findByDataComanda", query = "SELECT c FROM Comanda c WHERE c.dataComanda = :dataComanda")
    , @NamedQuery(name = "Comanda.findByEnfermeira", query = "SELECT c FROM Comanda c WHERE c.enfermeira = :enfermeira")
    , @NamedQuery(name = "Comanda.findBySala", query = "SELECT c FROM Comanda c WHERE c.sala = :sala")})
public class Comanda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idComanda")
    private Integer idComanda;
    @Column(name = "dataComanda")
    @Temporal(TemporalType.DATE)
    private Date dataComanda;
    @Column(name = "enfermeira")
    private String enfermeira;
    @Column(name = "sala")
    private Character sala;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "comandaId")
    private Collection<DetalheComanda> detalheComandaCollection;

    public Comanda() {
    }

    public Comanda(Integer idComanda) {
        this.idComanda = idComanda;
    }

    public Integer getIdComanda() {
        return idComanda;
    }

    public void setIdComanda(Integer idComanda) {
        this.idComanda = idComanda;
    }

    public Date getDataComanda() {
        return dataComanda;
    }

    public void setDataComanda(Date dataComanda) {
        this.dataComanda = dataComanda;
    }

    public String getEnfermeira() {
        return enfermeira;
    }

    public void setEnfermeira(String enfermeira) {
        this.enfermeira = enfermeira;
    }

    public Character getSala() {
        return sala;
    }

    public void setSala(Character sala) {
        this.sala = sala;
    }

    @XmlTransient
    public Collection<DetalheComanda> getDetalheComandaCollection() {
        return detalheComandaCollection;
    }

    public void setDetalheComandaCollection(Collection<DetalheComanda> detalheComandaCollection) {
        this.detalheComandaCollection = detalheComandaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComanda != null ? idComanda.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Comanda)) {
            return false;
        }
        Comanda other = (Comanda) object;
        if ((this.idComanda == null && other.idComanda != null) || (this.idComanda != null && !this.idComanda.equals(other.idComanda))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Comanda[ idComanda=" + idComanda + " ]";
    }
    
}
